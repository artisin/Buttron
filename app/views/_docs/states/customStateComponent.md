#### Custom State Component
The `customState` object is the only `state` that gives you the option to specify a `component` to be attached to said `state`. Which gives you a convinent way to define `components` that have logic which is tied to a `state`. Note, since we are working with a `customState` we must tell Buttron to apply transtions.

> __SideBar:__ If you wish to pass two seperate `customState`'s you must prefix the key such as `customState2` otherwise only one `customState` will be registered.

__HTML Markup__
```html
<div class="superPowers">
  input(type="checkbox" name="radioMe")
  <span>
    Super
    <span>Powers</span>
  </span> 
</div>
```

__Stylus Markup__
```styl
.superPowers
  buttron({
    style: false
    position: absolute
    width: 200px
    height: 45px
    component: {
      input:{
        position: absolute
        z-index: 10
        margin-left: 0px
        top: 5px
        left: 0
        height: 25px
        width: 25px
        //Apply trans to: input
        transition: true
        //Custom State
        customState:{
          //State type
          state: "checked"
          margin-left: 10px
          //Apply trans to: input:checked
          transition: true
          //Checked span component 
          //input:checked ~ span
          component:{
            span:{
              //Specify selector
              selector: "~"
              background: alpha(gray, 0.2)
              border-top: 5px #27ae60 solid
              border-radius: 5px
              padding-left: 45px
              //Apply trans to: input:checked ~ span
              transition: true
              component:{
                span:{
                  color: #27ae60
                  letter-spacing: 3px
                  //Apply trans to: input:checked ~ span > span
                  transition: true
                }
              }
            }
          }
        }
      }
      //Non-checked span state
      span:{
        border-top: 5px gray solid
        padding-top: 5px
        padding-left: 35px
        padding-bottom: 5px
        padding-right: 5px
        //Apply trans to: span
        transition: true
        component:{
          span:{
            //Apply trans to: span > span
            transition: true
          }
        }
      }
    }
  })
```


__CSS Output__
```css
.superPowers {
  position: absolute;
  width: 200px;
  height: 45px;
}
.superPowers > input {
  position: absolute;
  z-index: 10;
  margin-left: 0px;
  top: 5px;
  left: 0;
  height: 25px;
  width: 25px;
}
.superPowers > input {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.superPowers > input:checked ~ span {
  background: rgba(128,128,128,0.2);
  border-top: 5px #27ae60 solid;
  border-radius: 5px;
  padding-left: 45px;
}
.superPowers > input:checked ~ span {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.superPowers > input:checked ~ span > span {
  color: #27ae60;
  letter-spacing: 3px;
}
.superPowers > input:checked ~ span > span {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.superPowers > input:checked {
  transition-property: margin-left;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  margin-left: 10px;
}
.superPowers > span {
  border-top: 5px #808080 solid;
  padding-top: 5px;
  padding-left: 35px;
  padding-bottom: 5px;
  padding-right: 5px;
}
.superPowers > span {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.superPowers > span > span {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}

```