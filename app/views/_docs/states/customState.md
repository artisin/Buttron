### Custome State 
The `customState` object gives you the ablity to add and create any `state` the task calles for such as `:checked`. `customState` functions in the same way as the above hard-coded states do execpt you must specify a `state` key-pair within the `customState` object. By defualt custom states do not auto generate trasition styles.

__Stylus Markup__
```styl
buttron({
  customState:{
    //Specify the state without `:`
    state: <state>
  }
})
```

> __Sidebar:__ Pseudo elements such as `:after` and `:before` are not considered to be a `state` but rather an [`element`]() therefore even though you can technically specify `:after` as your state selector that is not its intended pourpose.


For example lets create a `:checked` state on a checkbox that makes the checkbox grow in size if checked. 
__Stylus Markup__
```styl
.checkMe input
  buttron({
    //Turn of the default styling
    style: false
    customState: {
      //Specify state (no ":" needed)
      state: checked
      //Turn on transitions since they are off by default
      transition: true
      height: 50px
      width: 50px
    }
  })
```

__CSS Output__
```css
.checkMe input {
  transition-property: height , width;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.checkMe input:checked {
  transition-property: height , width;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  height: 50px;
  width: 50px;
}
```
