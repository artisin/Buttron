### Apply To With A State 
All `applyTo` does is what you prbly assume it does, it applies said transition to whatever you specify. Case in point, lets say you want to you want a hover transition that that is only applied to the `span` element within your button.
__Stylus Markup__
```styl
.transApplyTo
  buttron({
    hover:{
      transition:{
        applyTo: span
        color: #F1C40F
        background: #8E44AD
        border-radius: 5px
        padding:1rem
      }
    }
  })
```
__CSS Output__
```css
.transApplyTo {
  //Defaults
}
.transApplyTo span {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.transApplyTo:hover span {
  transition-property: color , background , border-radius , padding;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  color: #f1c40f;
  background: #8e44ad;
  border-radius: 5px;
  padding: 1rem;
}
```