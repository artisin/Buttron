### Transition Option
Within `transition` you can use the `option` object which allows you to alter the [default transtion](#) option in the same fashion as you can with a `state`. 

```styl
option: {
  property: //..
  duration: //..
  ease: //..
  delay: //.. 
}
```
For `property` you must seperate your `properties` by a space and __not__ commas, Buttron will phrase it and add the commas for you. 
```styl
option:{
  property: background font-size
}
//alternitivly use strings
option:{
  property: "background" "font-size"
}
```

> _SideBar_: I recomend you limit the use of `option` within `transitions` for cases in which you would like to alter soley the transition properties as I demenstarte in the [longInShortOut]() example. Otherwise,  I recommend you use the `globalOption` which I will cover latter on. 

__Stylus Markup__
```styl
.optionDemo
  buttron({
    transition:{
      option:{
        property: background font-size color
        durration: 2.5s
      }
    }
  });
```
__CSS Output__
```css
.optionDemo {
  //Defaults
}
.optionDemo {
  transition-property: background , font-size , color;
  transition-duration: 2.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
```

#### Shorthand
Specifying option is all good and dandy but inheritly limited and quickly becomes useless when you are trying to compose more complex transtions. Don't you fear becuase `shorthand` is here which allows you to create `shorthand` css transition notaition with ease. 
__Syntax Formate__
```styl
shorthand:{
  <property>: <duration> <timing-function> <delay>
  <property>: <duration> <timing-function> <delay>
}
```

__Stylus Markup__
```styl
.shorthandDemo
  buttron({
    transition:{
      option:{
        shorthand:{
          background: 2s
          color: 3s ease-in
          border: 1s ease-out 2s
        }
      }
    }
  });
```

__CSS Output__
```css
.shorthandDemo {
  //Defaults
}
.shorthandDemo {
  transition: background 2s , color 3s ease-in , border 1s ease-out 2s;
}
```