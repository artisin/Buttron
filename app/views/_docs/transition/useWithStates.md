### Basics Use With State
Lets say you want to make a `hover` effect that has a long fade in _`3s`_ and short fade out _`0.3s`_ you will need to leverage the `transition` to do so.
__Stylus Markup__
```styl
.shortInLongOut
  buttron({
    //Applied to parent which is `.shortInLongOut`
    transition:{
      option:{
        duration: 3s
        //If I do not specify properties it will defualt to all
        property: background
      }
    }
    hover: {
      background: #E74C3C
      option: {
        duration: 0.3s
      }
    }
  });
```
__CSS Output__
```css
.shortInLongOut {
  //Defaults
}
.shortInLongOut {
  transition-property: background;
  transition-duration: 3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.shortInLongOut:hover {
  transition-property: background;
  transition-duration: 0.3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  background: #e74c3c;
}

```