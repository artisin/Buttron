Buttron ToDo
------------
- __Transistion__
  [ ] Global
  + [x] Basic
  + [x] Option
    * [x] Default Method
    * [x] Shortand
      + [x] Default key word 
    * Special
      - [x] ApplyTo
      - [ ] Root
  + [x] Object Notation
    * [x] Option A
    * [x] Option B
    * [X] Option C
    * [x] GlobalOptions
- __Animation__
  + [ ] Basic
  + [ ] Options
    * [ ] Default
    * [ ] Special
      - [ ] ApplyTo
      - [ ] Root
  + [ ] ObjectNotation (multiple timelines)
    * [ ] Options
      - [ ] ApplyTo
      - [ ] Root
    * [ ] Shorthand
      - [ ] Option A
      - [ ] Option B
  + [ ] Inherit Timeline
- __State__
  + [x] Basic States
  + [ ] Custom State
    * [ ] .....
  + [ ] Transition
    * [x] AutoGen for Global 
    * [ ] Option
      - [x] Default
      - [x] Shorthand
      - [ ] Special
        + [x] ApplyTo
        + [ ] Root
    * [x] On/Off transtion booleans
    * [x] Object Notation
    * [x] State Overright


+ Ideas
  <!-- * autogen master on or off -->
  * syntax
    - boolean  $$<>
    - obj $<>
    - string _
    - itterator -> nacked
    - funk -> nacked
    - global helpers -> g_
    - top level - cap
  * timeline vars, autocalced