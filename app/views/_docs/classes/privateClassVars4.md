#### What About The Maths??
To evaluate variables that use math you can use the Buttron's custom `math()` function inside of `eval`. After two morning coffee/sits I decied the best way to apporch this problem was to let the browser to the heavy lifting and evaluate said maths inside of `calc` rather than attempting to roll out my own custom solution. Thus allowing you not only to specify your prefix but also giving you the power to reffrence other static and math varables alike. 

That being said, it might be in your best intrest to brush up on your [calc](https://developer.mozilla.org/en-US/docs/Web/CSS/calc) knowledge.

__Stylus Markup__
```styl
evalMaths = ButtronClass('evalMaths', {
  $$:{
    eval:{
      height: "math($$[base] * 20px / 2)"
      width: "math($$[height] / 0.5)"
      hoverT: "math(($$[base] - ($$[base] - 1)) * 1s)"
    }
    base: 10
  }
  height: "$$[height]"
  width: "$$[width]"
  hover:{
    background: #F1C40F
    option:{
      duration: "$$[hoverT]"
    }
  }
})

.evalMaths
  evalMaths()
```

__CSS Output__
```css
.evalMaths {
  //Defaults
  height: calc(10 * 20px / 2);
  width: calc((10 * 20px / 2) / 0.5);
}
.evalMaths {
  //Defaults
  transition-duration: calc((10 - (10 - 1)) * 1s);
}
.evalMaths:hover {
  //Defaults
  transition-duration: calc((10 - (10 - 1)) * 1s);
  background: #f1c40f;
}
```

