### Private Class Varibles
Using global stylus variables in your `class` is fine and dany,but lets say you want to create a Buttron class that allows you or other users to __easliy__ customize the colors or anyother property. This is where private varibles come in handy becuase they give you the ablity to have locolized private class variables which then can be changed without affecting the global stylus varaible scoop. 

To initilize private varibles you must create a object with the key of `$$` and then list your variables inside. Then you can reffrence those varibles using a the string notation of: `$$[<variable>]`. It is critical you specify these variables as strings and you use bracket notation.

#### Basic Usage
__Stylus Markup__
```styl
privateVars = ButtronClass('privateVars', {
  $$:{
    //List varibles
    bgColor: #1ABC9C
    borderWidth: 5px
    borderColor: #9B59B6
    fz: 1.2rem
  }
  background: "$$[bgColor]"
  font-size: "$$[fz]"
  border: "$$[borderWidth]" solid "$$[borderColor]"
})

.myPrivateButton
  privateVars()
```

__CSS Output__
```css
.myPrivateButton {
  //Defaults..
  background: #1abc9c;
  font-size: 1.2rem;
  border: 5px solid #9b59b6;
}

```




