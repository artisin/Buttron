### The Basics
Buttron foundation and orging lies in its ablity to create and or import classes. And a `class` is simply a reffrence to Buttron instance that is tied to a stylus variable which you can then invoke and place or time you wish within your project.

#### Basic Usage
__Stylus Markup__
```styl
//Create Instance
<class> = ButtronClass("<class>", {
  //Class Styles
})

//Use Instance//
//Decarative
#anID
  <class>()
//Imperative
<class>("#anID")
```

__CSS Output__
```css
#anID {
  //Class Styles
}
```

#### Why Use Classes?
For the same reason you use css classes, orginiztion. Buttron classes give you the ablity to define all your logic in one centralized location while also giving you the flexablity to alter said class as desired.

Consider the following senerio. You are building a website and all your buttons are blue with large font. Without utilizing a Buttron class you would be forced to litter your project with the following code.
```styl
.buttonOne
  buttron({
    background: blue
    font-size: 2rem
  })
```
While this would work but it's not DRY and what if you decied you want your font-size to be a bit bigger or your blue to be a bit darker? As its stands you would have to go through all your code and make this change to each seperate instance witch could be a sinch or a nightmare. _I hope you get where I am headed with this._ So rather than copy and pasting the above snippet of code over and over again lets make a `class`!

#### Creating a Class
Creating classes is a easy three step process.
1. Create a stylus variable _(to create a reffrance)_.
2. Assign said variable to a `ButtronClass()` constructor
3. Within the constructor your first argument will be your stylus variable in a `string` formate, followed by a object containg the logic of your new class

> __Sidebar:__ The reason you need to declare the assigned variable within the constructor is to store a reffrence to the class withing the global `_Buttron` object. That being said, 

__Stylus Markup__
```styl
//Create Instance
bigBlueBtn = ButtronClass('bigBlueBtn', {
  background: blue
  font-size: 2rem
})

//Use Instance
.blueBtnOne
  bigBlueBtn()
```

__CSS Output__
```css
.blueBtnOne {
  //Defaults..
  background: #00f;
  font-size: 2rem;
}
```


