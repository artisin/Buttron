So if you where to change the `font-size` in the `bigBlueBtn` `class` from `2rem` to `1.5rem` every selector which you applied the `bigBlueBtn` instance to will inherit the new `font-size`. 

#### But What If One Of My Buttons... 
Needs to have yellow font and a lighter shade of blue? No problem, you can still use the `bigBlueBtn` `class` and just specify the altered property within `bigBlueBtn` just as you would with a `buttron` instance and those properties will overwright the default `bigBlueBtn` `class` properties. 
__Stylus__
```
.otherBtn
  bigBlueBtn({
    background: lighten(blue, 20%)
    color: yellow
  })
```
