#### Change Private Varibles
To change private varibles is as easy as changing any other property, just specify the new varible values and Buttron will merge the new arguments.

>__Sidebar:__ If you rather overwrite the `$$` object rather than merge you just specify `merge: false` within the `$$` object. 

__Stylus Markup__
```styl
.myNewPrivateButton
  privateVars({
    $$:{
      borderColor: #E74C3C
      fz: 1.5rem
    }
  })

```

__CSS Output__
```css
.myNewPrivateButton {
  //Defaults..
  background: #E74C3C;
  font-size: 1.5rem;
  border: 6px solid #9b59b6;
}

```