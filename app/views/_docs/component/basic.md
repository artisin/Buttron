### The Basics
The component object lets your target children elements of the parent.
```html
<div class="myButton"> <!-- Parent -->
  <span>A Buttron</span> <!-- Child element of .myButton -->
  <div class="icon"> <!--Child element of .myButton -->
    <div class="githubIcon"></div> <!-- child element of .icon -->
  </div>
</div>
```
The component 

+ element, or default as key
+ selector, `>` defaults, or specified
+ applyTo: typical



###Trans
+ state
  * root