### Custom Elements
The `customElement` object gives you the ablitiy to add and create any `element` the task calles for such as `:first-letter `. A `customElement` functions in the same way as the above hard-coded `elements` do execpt you must specify a `element` key-pair within the `customElement` object.

__Stylus Markup__
```styl
buttron({
  customElement:{
    element: <element>
  }
})
```

You are not limited to a single `element` and have the ablity to pass mutiple `elements` which is convinet if two `element`'s share the same styles such as `:after` and `:before`

>__Sidebar:__ If you wish to pass two seperate `customElement`'s you must prefix the key such as `customElement2` otherwise only one `customElement` will be registered. For those who care, all Buttron does is cylce through the keys of the object and does a `regex.match` for `-child|-type|customElement`. 

__Stylus Markup__
```styl
$c1 = #E74C3C
$c2 = #9B59B6
.multipleElm
  buttron({
    customElement:{
      element: before after
      content: ''
      position: absolute
      z-index: -10
      width: 100%
      height: 100%
    }
    after:{
      left: 50%
      background: $c1
    }
    before:{
      right: 50%
      background: $c2
    }
    //Important: you must prefix key if you have multiple customElement's specified 
    customElement2:{
      element: first-letter
      font-size: 2rem
      color: #3498DB
    }
  })
```

__CSS Output__
```css
.multipleElm {
  //Defaults
}
.multipleElm:before {
  content: '';
  position: absolute;
  z-index: -10;
  width: 100%;
  height: 100%;
}
.multipleElm:after {
  content: '';
  position: absolute;
  z-index: -10;
  width: 100%;
  height: 100%;
}
.multipleElm:after {
  left: 50%;
  background: #e74c3c;
}
.multipleElm:before {
  right: 50%;
  background: #9b59b6;
}
.multipleElm:first-letter {
  font-size: 2rem;
  color: #3498db;
}

```
