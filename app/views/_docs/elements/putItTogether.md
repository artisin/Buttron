### Custom Elements
The `customElement` object gives you the ablitiy to add and create any `element` the task calles for such as `:first-of-type`. A `customElement` functions in the same way as the above hard-coded `elements` do execpt you must specify a `element` key-pair within the customState object.

__Stylus Markup__
```styl
buttron({
  customElement:{
    element: <element>
  }
})
```

> __Sidebar:__ You are able to pass the `first-child` `element` selector as is without any issues but any `element` selector that contains parentheses such as `:nth-child(2)` you need to pass it as a string. 

#### Example
Alright, at first glance this may seem like a fair amount of code and it is, although, its just the same snipit of code over and over so really not all to complicated. Additionally, as you can see you free to use your own stylus veriables withing Buttron.

__HTML__
```html
<div class="customElm">
  <span></span>
  <span>Cool!</span>
  <span></span>
</div>
```

__Stylus Markup__
```styl
//Custom Variables
$baseTime = 0.5s
$c1 = #1ABC9C
$c2 = #2ECC71
$c3 = #9B59B6
$c4 = #34495E
$c5 = #F1C40F
$c6 = #E74C3C
//Create a Buttron Class
coolDemo = createButtronClass('coolDemo', {
  position: absolute
  component:{
    span:{
      //First
      first-child:{
        before:{
          content: ''
          z-index: -15
          position: absolute
          background: transparent
          width: 0%
          height: 0%
          top: 0
          left: 0
          //Out trans
          transition:{
            option:{
              //2.5s
              delay: 5 * $baseTime
            }
          }
          hover:{
            //Apply to root so the hover trigger is on the button
            root: true
            width: 100%
            height: 100%
            background: alpha($c1, 0.75)
          }
        }
        after:{
          content: ''
          z-index: -15
          position: absolute
          background: transparent
          width: 0%
          height: 0%
          top: 0
          right: 0
          //Out trans
          transition:{
            option:{
              //2s
              delay: 4 * $baseTime
            }
          }
          hover:{
            root: true
            width: 100%
            height: 100%
            background: alpha($c2, 0.75)
            option:{
              //0.5s
              delay: 1 * $baseTime
            }
          }
        }
      }
      //Second 
      "nth-child(2)":{
        //For text to show
        z-index: 10
        //Text hover
        hover:{
          root: true
          font-size: 2.5rem
          option:{
            duration: $baseTime * 6
          }
        }
        before:{
          content: ''
          z-index: -10
          position: absolute
          background: transparent
          width: 0%
          height: 0%
          top: 0
          left: 0
          //Out trans
          transition:{
            option:{
              //1.5s
              delay: 3 * $baseTime
            }
          }
          hover:{
            root: true
            width: 100%
            height: 100%
            background: alpha($c3, 0.75)
            option:{
              //1s
              delay: 2 * $baseTime
            }
          }
        }
        after:{
          content: ''
          z-index: -10
          position: absolute
          background: transparent
          width: 0%
          height: 0%
          top: 0
          right: 0
          //Out trans
          transition:{
            option:{
              //1s
              delay: 2 * $baseTime
            }
          }
          hover:{
            root: true
            width: 100%
            height: 100%
            background: alpha($c4, 0.75)
            option:{
              //1.5s
              delay: 3 * $baseTime
            }
          }
        }
      }
      customElement:{
        element: "nth-child(3)"
        before:{
          content: ''
          z-index: -10
          position: absolute
          background: transparent
          width: 0%
          height: 0%
          top: 0
          left: 0
          //Out trans
          transition:{
            option:{
              //0.5s
              delay: 1 * $baseTime
            }
          }
          hover:{
            root: true
            width: 100%
            height: 100%
            background: alpha($c5, 0.75)
            option:{
              //2s
              delay: 4 * $baseTime
            }
          }
        }
        after:{
          content: ''
          z-index: -10
          position: absolute
          background: transparent
          width: 0%
          height: 0%
          top: 0
          right: 0
          //Out trans
          transition:{
            option:{
              delay: 0s
            }
          }
          hover:{
            root: true
            width: 100%
            height: 100%
            background: alpha($c6, 0.75)
            option:{
              //2.5s
              delay: 5 * $baseTime
            }
          }
        }
      }
    }
  }
})

//Apply coolDemo Buttron Class to .customElm
.customElm
  coolDemo()
```


__CSS Output__
```css
.customElm {
  //Defualts
}
.customElm:hover > span:first-child::before {
  transition-property: width , height , background;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  width: 100%;
  height: 100%;
  background: rgba(26,188,156,0.75);
}
.customElm > span:first-child:before {
  transition-property: width , height , background;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 2.5s;
  content: '';
  z-index: -15;
  position: absolute;
  background: transparent;
  width: 0%;
  height: 0%;
  top: 0;
  left: 0;
}
.customElm > span:nth-child(2) {
  transition-property: font-size;
  transition-duration: 3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  z-index: 10;
}
.customElm:hover > span:nth-child(2) {
  font-size: 2.5rem;
}
.customElm > span:nth-child(2) {
  transition-property: font-size;
  transition-duration: 3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  z-index: 10;
}
.customElm:hover > span:first-child::after {
 //..
}
.customElm > span:first-child:after {
 //..
}
.customElm:hover > span:nth-child(2)::before {
 //..
}
.customElm > span:nth-child(2):before {
 //..
}
.customElm:hover > span:nth-child(2)::after {
 //..
}
.customElm > span:nth-child(2):after {
 //..
}
.customElm:hover > span:nth-child(3)::before {
 //..
}
.customElm > span:nth-child(3):before {
 //..
}
.customElm:hover > span:nth-child(3)::after {
 //..
}
.customElm > span:nth-child(3):after {
 //..
}
```