### Before And After State Interaction
These two pseudo `elements` interact slightly diffrent with `states` in that a pseudo `element` will be attached to the `state` which accordinly is attached to the parent becuase you cannot apply a `state` to a pseudo `element`.

>__Sidebar:__If you wish to disable or apply this `state` interaction to other elements you can respectivly pass `pseudo: false` or `pseudo: true` in the object body.

#### Hover Example
__Stylus Markup__
```styl
.pseudoHover
    after:{
      //Styles..
      hover:{
        background: #2ECC71
        left: calc(100% - 20px) 
      }
    }
    before:{
      //Styles..
      hover:{
        background: #2ECC71
        right: calc(100% - 20px) 
      }
    }
```

__CSS Output__
```css
.pseudoHover {
  //Defaults
}
.pseudoHover:hover:after {
  transition-property: background , left;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  background: #2ecc71;
  left: calc(100% - 20px);
}
.pseudoHover:after {
  //Styles...
  transition-property: background , left;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.pseudoHover:hover:before {
  transition-property: background , right;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  background: #2ecc71;
  right: calc(100% - 20px);
}
.pseudoHover:before {
  //Styles..
  transition-property: background , right;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
```