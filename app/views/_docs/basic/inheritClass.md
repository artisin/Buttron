### Inherit Class
While you can specify classes most users will opt to use the inherit feture since it can be seamlessly applied within your typical stylus paradigm. In addition, inherit can leveage to power of stylus `cache` feture. That being said, all my examples will use the inherit formate so I think its benifical we get some things straight with a few code examples. The key thing to remeber is that Buttron will inherit all lines above it but only target the immediate line above it.

__Stylus Markup__
```styl
.inherit
  buttron()
#inherit
  buttron()
input
  buttron()
input:first-child
  buttron()
:first-of-type
  buttron()
.add + #me
  buttron()
div ~ p:nth-child(2) > .cool
  buttron()
```

__CSS Output__
```css
.inherit {
  //Defaults..
}
#inherit {
  //Defaults..
}
input {
  //Defaults..
}
input:first-child {
  //Defaults..
}
:first-of-type {
  //Defaults..
}
.add + #me {
  //Defaults..
}
div ~ p:nth-child(2) > .cool {
  //Defaults..
}
```

Alright I hate to beat a dead horse but not all of us are CSS wisards so one more example.

__HTML Markup__
```html
<div class="firstLine">
  <p>What</p>
  <div class="secondLine">
    <span>Will</span>
  </div>
  <div id="secondLine">
    <p>Button</p>
    <div class="thirdLine">
      <span>Do?</span>
    </div>
  </div>
</div>
```

__Stylus Markup__
```styl
.firstLine
  font-weight: normal
  p
    margin: 0
    pardding: 0
  .secondLine
  #secondLine
    .thirdLine
      buttron()
```

__CSS Output__
```css
.firstLine {
  font-weight: normal;
}
.firstLine p {
  margin: 0;
  pardding: 0;
}
.firstLine .secondLine .thirdLine,
.firstLine #secondLine .thirdLine {
  //Defaults..
}
```