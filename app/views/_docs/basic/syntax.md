### Syntax
Now you have to remeber this plugin was written in `stylus` and although the syntax is `javascript`-like its still `stylus` which is then phrased into `javascript`. If there is an error `stylus` will let you know however at times these errors can be somewhat ----. I do not have a diffinitave guide for you just some recommendations.

1. Just forget semicolins`;` don't even think about using they will cause all sorts of errors. If you need and or desire to have some closing charater on your properties use a comma
2. Objects don't need commas to be seperated, although, I recommend you use them for clarity.
3. Don't overthink or force it. If you find yourself cursing at the new and old Gods your doing something wrong. Remeber at the end of the day this is just a __tool__ and not a complete __solution__. That being said, have no shame in using Buttron to generate the basic framework of your button and then customizing that code as you see fit.
4. This does not really fit under syntax but if you want to add a feture to Buttron, have a recomendation, or found a major flaw, your best bet is just to drop me a line. I throw Buttron together over a few days and acordingly the code is wild and my conventions are inconsistant but at the end of the day it works. Plus, I wrote a hundred tests or so. 
