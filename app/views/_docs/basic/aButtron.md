### A Buttron
The basic markup needed to create a buttron is simple, you can either specify a selector or inherit a selector. The buttron will then use set defaults to generate defualt styles.

__Stylus Markup__
```styl
//Specified
buttron('<selector>');

//Inherit
<selector>
  buttron()
```

#### Example

__Html MarkUp__
```
<div class="aButtron">
	<span>
		A Buttron
	</span>
</div>
```

__Stylus Markup__
```styl
//Specified
buttron('.aButtron')

//Inherit 
.aButton
  buttron()
```