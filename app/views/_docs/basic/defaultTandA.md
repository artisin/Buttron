### Global Default Transition and Animation Properties
Transitons and animation have defaults as well for your convinces. Accordingly, if you do not specify a property within your transition or animation the property will be set as the defualt. Both transition defualt and animation defualts will inherit the global defualts, although, local defualts will always overwright global defaults.

`local > global > default`

####Global Defaults
+ `$duration` = 0.5s
+ `$ease` = ease-in-out
+ `$delay` = 0s

#### Transition Defualts
+ `transition-duration`: `$duration`
+ `transition-timing-function`: `$ease`
+ `transition-delay`: `$delay` 

#### Animation Defualts
+ `animation-duration`: `$duration`
+ `animation-timing-function`: `$ease`
+ `animation-delay`: `$delay`
+ `animation-iteration-count`: 1
+ `animation-direction`: normal
+ `animation-fill-mode`: initial
+ `animation-play-state`: initial

#### State Defaults
State transition such as `hover` and `active` inherit the transition defualts if specified otherwise they defualt to the global defualts. Pseudo defualts opperate in the same fashion as state defualts and you can change pseudo defaults through the `pseudo` object within the `default` object.

#### How to set Transition and Animation Defualts
To set the defualts all you need to to is specify those defualts within the `defualt` object.
__Changing The Defualts__
```styl
.aButtron
  buttron({
    default:{
      //Globals
      duration: //..
      ease: //..
      delay: //..
      transition: {
        //.. 
      },
      animation: {
        //..
      }
      //States
      hover: {
        //..
      },
      active: {
        //..
      },
      focus: {
        //..
      },
      visited: {
        //..
      },
      pseudo: {
        //..
      }
    }
  })
```
_This will all make more sense once you get a handle on appling transitions and animations_