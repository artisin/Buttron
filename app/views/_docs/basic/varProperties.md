### Variables Properties
Since Buttron was written in Stylus you can take full advantage of everthing Stylus has to offer from built-in functions to variables and everything else in-between.* 

__Stylus Markup__
```styl
//Color var
$color = blue
$width: 300px
.varButton
  buttron({
    background: alpha(darken($color, 20%), 0.8)
    width: $width
  })
```

__CSS Output__
```css
.varButton {
  //Defaults
  background: rgba(0,0,204,0.8);
  width: 300px
}
```

*I have not actually tested everything in-between nor do I plan to any time soon.