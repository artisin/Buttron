var gulp       = require('gulp'),
    del        = require('del'),
    config     = require('../config'),
    jadeConfig = require('../config/jade'),
    iconConfig = require('../config/iconFont');

gulp.task('clean', function (cb) {
  var compiledTests = config.sourceAssets  + '/js/__tests__/cases/compiled';
  del([
    config.publicDirectory,
    compiledTests,
    iconConfig.stylusDest,
    jadeConfig.dest,
  ], cb);
});
