var config        = require('./'),
    webpackConfig = require('./webpack')('test');

module.exports = {
  frameworks: ['mocha', 'sinon-chai'],
  files: [
    config.sourceAssets + '/js/__tests__/**/*.js',
    config.sourceAssets + '/js/__tests__/**/*.styl',
    config.sourceAssets + '/js/__tests__/**/*.css',
  ],
  exclude:[
    config.sourceAssets + '/js/__tests__/**/*.txt',
  ],
  watch: [
    //Watch for txt desc to run test
    config.sourceAssets + '/js/__tests__/**/*.txt'
  ],
  preprocessors: {
    'app/assets/js/__tests__/*': ['webpack', 'sourcemap'],
    'app/assets/js/__tests__/**/*.styl': ['stylus']
  },
  stylusPreprocessor: {
    options: {
      paths: ['app/assets/styles/_base/buttron'],
      save: true
    },
    transformPath: function(path) {
      path = path.replace(/\.styl$/, '.compiled.css');
      path = path.replace(/\/cases\//g, '/cases/compiled/');
      return path;
    }
  },
  webpack: webpackConfig,
  //Bumped up to comp for buttron complie time
  browserNoActivityTimeout: 100000,
  singleRun: true,
  reporters: ['nyan'],
  colors: true,
  browsers: ['PhantomJS_custom'],
    // you can define custom flags
    customLaunchers: {
      'PhantomJS_custom': {
        base: 'PhantomJS',
        options: {
          windowName: 'my-window',
          settings: {
            webSecurityEnabled: false
          },
        },
        flags: ['--load-images=false'],
        debug: false
      }
    },

    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    }
};